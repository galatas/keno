package com.mybet.keno;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.*;

public class TableList {

    public void start() {
        JSONParser jsonParser = new JSONParser();
        File dirResources = new File(this.getClass().getResource("/tables/").getFile());
        File[] listOfTables = dirResources.listFiles();
        if(listOfTables == null) {
            System.out.println("Table list is empty");
            return;
        }
        Arrays.stream(listOfTables).
            filter(file -> file.isFile() && file.getName().endsWith(".json")).
            map(file -> {
                try (Reader tableReader = new FileReader(dirResources + "/" + file.getName())) {
                    JSONObject jsonObj = (JSONObject) jsonParser.parse(tableReader);
                    return new TableBean(
                            (String) jsonObj.get("tableName"),
                            (Long) jsonObj.get("timeRun"),
                            (long) jsonObj.get("maxNumbers"),
                            (long) jsonObj.get("maxDraw"));
                } catch (Throwable e) {
                    e.printStackTrace();
                    return null;
                }
            }).
            filter(Objects::nonNull).
            forEach(tableBean -> Keno.game(tableBean));
    }
}
