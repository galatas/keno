package com.mybet.keno;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Keno {
    private static final int MIN_NUMBER = 1;
    private static final String DIRECTORY = "/home/user/src/Keno/src/";
    private static final String LINE_SEPERATOR = System.getProperty("line.separator");

    public static void game(TableBean bean) {
        List<Long> NUMBERS = IntStream.rangeClosed(MIN_NUMBER,bean.getMaxNumbers().intValue()).mapToObj(i -> (long)i).collect(Collectors.toList());
        JSONParser jsonParser = new JSONParser();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                File dir = new File(DIRECTORY);
                File[] listOfFiles = dir.listFiles();
                if (listOfFiles == null)
                    return;
                Random rnd = new Random();
                List<Long> drawnNumbers = getComputerNums(rnd, bean.getMaxDraw(),NUMBERS);
                HashSet<Long> drawnNumbersSet = new HashSet<>(drawnNumbers);
                Arrays.stream(listOfFiles).
                        filter(file -> file.isFile() && file.getName().endsWith(".json")).
                        map(file -> {
                            try (Reader r = new FileReader(DIRECTORY + file.getName())) {
                                JSONObject jsonObject = (JSONObject) jsonParser.parse(r);
                                JSONArray slideContent = (JSONArray) jsonObject.get("numbers");
                                return new BetBean(
                                        rnd.nextLong(),
                                        (String) jsonObject.get("username"),
                                        (String) jsonObject.get("table"),
                                        (Double) jsonObject.get("amount"),
                                        (Long) jsonObject.get("multiplier"),
                                        (Long) jsonObject.get("draws"),
                                        IntStream.range(0, slideContent.size()).mapToLong(i -> (long) slideContent.get(i)).toArray());
                            } catch (Throwable e) {
                                e.printStackTrace();
                                return null;
                            }
                        }).
                        filter(Objects::nonNull).
                        filter(betBean -> {
                            if(!betBean.getUsersTableName().equals(bean.getTableName())){
                                return false;
                            }
                            if (betBean.getAmount() < 0.5) {
                                System.out.println("\033[0;1mServer Response : {\"status\" : \"error\", \"message\" : \"Amount must be more that 0.5\"}\033[0;0m");
                                return false;
                            }
                            if (betBean.getMultiplier() < 1) {
                                System.out.println("\033[0;1mServer Response : {\"status\" : \"error\", \"message\" : \"Multiplier must be more that 1\"}\033[0;0m");
                                return false;
                            }
                            if (betBean.getDraws() < 1) {
                                System.out.println("\033[0;1mServer Response : {\"status\" : \"error\", \"message\" : \"Draws must be more that 1\"}\033[0;0m");
                                return false;
                            }
                            if (betBean.getNumbers().length > bean.getMaxDraw()) {
                                System.out.println("\033[0;1mServer Response : {\"status\" : \"error\", \"message\" : \"Too many numbers\"}\033[0;0m");
                                return false;
                            }

                            if (Arrays.stream(betBean.getNumbers()).distinct().count() != betBean.getNumbers().length) {
                                System.out.println("\033[0;1mServer Response : {\"status\" : \"error\", \"message\" : \"Numbers must be unique\"}\033[0;0m");
                                return false;
                            }

                            if (Arrays.stream(betBean.getNumbers()).filter(i -> i >= MIN_NUMBER && i <= bean.getMaxNumbers()).count() != betBean.getNumbers().length) {
                                System.out.println("\033[0;1mServer Response : {\"status\" : \"error\", \"message\" : \"Numbers must be from " + MIN_NUMBER + " to " + bean.getMaxNumbers() + "\"}\033[0;0m");
                                return false;
                            }

                            return true;
                        }).forEach(betBean ->{
                            Winnings.winnings(betBean, drawnNumbersSet);
                            StringBuilder builder = new StringBuilder().
                                append("Lottery Numbers for table \"").
                                append(bean.getTableName()).
                                append("\": ");
                                drawnNumbers.forEach(aLong -> builder.append(aLong + " "));
                                System.out.println(builder.append(LINE_SEPERATOR).toString());
                        });
            }
        }, 0, bean.getTimeRun());
    }

    private static List<Long> getComputerNums (Random random,Long numbersToDraw,List<Long> maxNumbers){
        Collections.shuffle(maxNumbers, random);
        return maxNumbers.subList(0, numbersToDraw.intValue());
    }
}
