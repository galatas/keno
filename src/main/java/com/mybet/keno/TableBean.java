package com.mybet.keno;

public class TableBean {
    private final Long timeRun;
    private final String tableName;
    private final Long maxNumbers;
    private final Long maxDraw;

    public TableBean(String tableName, Long timeRun, Long maxNumbers, Long maxDraw) {
        this.timeRun = timeRun;
        this.tableName = tableName;
        this.maxNumbers = maxNumbers;
        this.maxDraw = maxDraw;
    }
    public String getTableName() {
        return tableName;
    }

    public Long getTimeRun() {
        return timeRun;
    }

    public Long getMaxNumbers() {
        return maxNumbers;
    }

    public Long getMaxDraw() {
        return maxDraw;
    }

}
