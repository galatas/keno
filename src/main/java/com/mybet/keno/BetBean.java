package com.mybet.keno;

public class BetBean {
    private final long ticketNumber;
    private final String userName;
    private final String tableName;
    private final double amount;
    private final long multiplier;
    private final long draws;
    private final long[] numbers;

    public BetBean(long ticketNumber, String userName, String tableName, double amount, long multiplier, long draws, long[] numbers) {
        this.ticketNumber = ticketNumber;
        this.userName = userName;
        this.tableName = tableName;
        this.amount = amount;
        this.multiplier = multiplier;
        this.draws = draws;
        this.numbers = numbers;
    }

    public String getUserName() {
        return userName;
    }

    public String getUsersTableName() {
        return tableName;
    }

    public double getAmount() {
        return amount;
    }

    public long getMultiplier() {
        return multiplier;
    }

    public long getDraws() {
        return draws;
    }

    public long[] getNumbers() {
        return numbers;
    }

    public long getTicketNumber() {
        return ticketNumber;
    }
}
