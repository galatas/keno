package com.mybet.keno;

import java.util.*;
import java.util.stream.Collectors;

public class Winnings {

    private static final String LINE_SEPERATOR = System.getProperty("line.separator");
    private static final double PAYOUT[][] = {
            {0, 2.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //1
            {0, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //2
            {0, 0, 2.5, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //3
            {0, 0, 1, 4, 100, 0, 0, 0, 0, 0, 0, 0, 0}, //4
            {0, 0, 0, 2, 20, 50, 450, 0, 0, 0, 0, 0, 0},//5
            {0, 0, 0, 1, 7, 50, 1600, 0, 0, 0, 0, 0, 0},//6
            {0, 0, 0, 1, 3, 20, 100, 5000, 0, 0, 0, 0, 0},//7
            {0, 0, 0, 0, 2, 10, 50, 1000, 15000, 0, 0, 0, 0},//8
            {0, 0, 0, 0, 1, 5, 25, 200, 4000, 40000, 0, 0, 0},//9
            {2, 0, 0, 0, 0, 2, 20, 80, 500, 10000, 100000, 0, 0},//10
            {2, 0, 0, 0, 0, 1, 10, 50, 250, 1500, 15000, 500000, 0},//11
            {4, 0, 0, 0, 0, 0, 5, 25, 150, 1000, 2500, 25000, 1000000},//12
    };

    public static void winnings(BetBean bean, Set<Long> drawnNumbers) {

        StringBuilder builder = new StringBuilder().
                append("Table : ").
                append(bean.getUsersTableName()).
                append(LINE_SEPERATOR).
                append("User : ").
                append(bean.getUserName()).
                append(LINE_SEPERATOR).
                append("Amount : ").
                append(bean.getAmount()).
                append(LINE_SEPERATOR).
                append("Muliplier : ").
                append(bean.getMultiplier()).
                append(LINE_SEPERATOR).
                append("Draws : ").
                append(bean.getDraws()).
                append(LINE_SEPERATOR).
                append("Numbers : ").
                append(Arrays.stream(bean.getNumbers()).boxed().collect(Collectors.toList())).
                append(LINE_SEPERATOR).
                append(LINE_SEPERATOR).
                append("\033[0;1mServer Response : {\"status\" : \"ok\", \"TicketNumber\" : \"").
                append(bean.getTicketNumber()).append("\", \"User\" : \"").
                append(bean.getUserName()).append("\"}\033[0;0m").
                append(LINE_SEPERATOR).
                append(LINE_SEPERATOR);

        Set<Long> foundNumber = getCatch(Arrays.stream(bean.getNumbers()).boxed().collect(Collectors.toSet()), drawnNumbers);
        builder = builder.append("Found ").
                append(foundNumber.size()).
                append(" number(s).").
                append(foundNumber).
                append(LINE_SEPERATOR).
                append(LINE_SEPERATOR);
        double winnings = PAYOUT[bean.getNumbers().length][foundNumber.size()] * bean.getAmount() * bean.getMultiplier();
        if (winnings > 0) {
            builder = builder.append("\033[0;1mServer Response : ").
                    append(LINE_SEPERATOR).
                    append("{").
                    append(LINE_SEPERATOR).
                    append("\"status\" : \"win\",").
                    append(LINE_SEPERATOR).
                    append("\"tickets\" : [{ ").
                    append(LINE_SEPERATOR).
                    append("\"ticketNumber\" : \"").
                    append(bean.getTicketNumber()).
                    append("\", ").
                    append(LINE_SEPERATOR).
                    append("\"amount\" : \"").
                    append(winnings).
                    append("\"").
                    append(LINE_SEPERATOR).
                    append("}]").
                    append(LINE_SEPERATOR).
                    append("}\033[0;0m");
        } else
            builder = builder.append("You didn't win.");

        System.out.println(builder.append(LINE_SEPERATOR).toString());
    }

    private static Set<Long> getCatch(Set<Long> playerArray, Set<Long> computed) {
        Set<Long> players = new HashSet<>(playerArray);
        players.retainAll(computed);
        return players;
    }
}
